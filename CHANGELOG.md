---
website: http://jorgebrunal.xyz/projects/anuket
---

# Anuket

Awesome Google Maps utils using JS

- Current version **3.0.c**


## 3.0.c (2021-03-12T15:02:05Z)

### New

- method `addGroupMarkers`

### Fix
- fix docs on draggable option
- fix ident using [prettier](https://prettier.io)


## 3.0.b (2020-06-25T18:12:26Z)

### New

- method `drawCircle`
- method `fitBounds`
- method `drawPolygon`
- method `drawPolyline`
- method `version`
- **geoJSON** plugin
- **decodePolyline** plugin
- **distancematrix** plugin
- **kml** plugin
- **osmMap** plugin
- **staticMap** plugin
- implements native [extend](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Classes/extends)
- fix ident using [prettier](https://prettier.io)

### Fix

- draggable and editable options on `addMarker`
- fix option onDragend, and return marker after create on `addMarker`
- fix options in markers, draggable and infoWindow, on `addMarker`
- `centerMap`, and added new method `drawPolygon`
- `addMarker` receive more options
- `osmMap`, is now a **plugin*+
- bug fixed in `geocoder` and `inverseGeocoder` methods
- now using **AnuketMap** in `setCenter` and `setZoom`

### Deprecated

the following methods are removed from future versions:
- `geocodePosition` (now use **getCurrentPosition**)


## 3.0.a (2018-05-15T12:36:48Z)

### New
- now Anuket is a **UMD** module
- added more options to `clearMap` method
- new method `removeLayer`
- new method `setLayer`
- roadmap to **3.0**
- update changelog
- using **package.json** to dependencies and test
- using **parcel-bundler** to build project

### Fix

- change initial address
- remove params from `getObjectMap` and `getAnuketObj`
- validate params on `geocodePosition`
- `drawRoute` now using several options
- fix `streetView` method, now using options
- fix `getZoom`, `clearMap`, `centerMap`

### Deprecated

the following methods are renamed from future versions:

- `getObjectMap`, in flavour of `getAnuketObj` method
- `getGoogleMap`, in flavour of `getGoogleObj` method
- `servinfMap`, in flavour of `getGoogleObj` method
- `osmMap`, in flavour of `getGoogleObj` method


## 2.0.9 (2018-03-25T05:29:49Z)

### New

- added **zoomControl**, and **zoomControlOptions** to the `Run` Method


## 2.0.8 (2017-12-06T02:25:33Z)

### New

- new method `getGoogleMap`
- reimplement`geocodePosition`
- added **StyledDark** Map without places


## 2.0.7 (2017-10-19T09:51:05Z)

### Fix

- remove deprecated functions and methods
- fix `drawRoute` function, now validate params, and added markers to origin and destiny


## 2.0.6 (2017-10-14T03:05:08Z)

### New

- added new methods `setMapType`, `osmMap`, `servinfMap`


## 2.0.5 (2017-10-12T08:03:03Z)

### New

- new option `geolocation` on `Run` Method
- update initial coords
- new method `centerOverOrigin`, `getOriginPosition`

### Fix

- validate this and use `getCurrentPosition` method, else use `latlong`

### Deprecated

the following methods are removed from future versions:

- `mapPos`
- `geocodePosition`
- `showLayer`
- `hideLayer`
- `showAll`
- `hideAll`
- `getConfigLayers`


## 2.0.4 (2017-10-10T15:25:49Z)

### New

- new fn `clearMap`
- check objects with params to the Run method
- now using `$.notify`

### Fix

- setCenter on init
- fix function `streetView`
- `addMarker` method now receive function `clickOnMarker` param for callback

### Deprecated

- remove cities


## 2.0.3 (2016-05-10T17:13:05Z)

### New

- `clearMap` now uses `removePolylines` and set init marker
- now center map when load
- `drawRoute` uses `removePolylines`

### Fix

- run method now using new object options
- change Materialize.toast to `$.notify`
- remove old methods `loadGeo`, `baseMap`


## 2.0.2 (2016-03-27T19:18:24Z)

### New

- now is **IIFE** module
- new variable system config
- new private config var for maps `AnuketMap`

### Fix

- remove support for fusion layer
- rewrite `addMarker` method


## 2.0.1 (2016-01-22T18:34:55Z)

### New

new methods:

- `fusionLayers`
- `mapPos`
- `geocodePosition`
- `addMarker`
- `streetView`
- `run`
- `loadMap`
- `loadGeo`
- `geoRef`


## 2.0.0 Initial app (2016-01-22T18:21:27Z)

### New

- added configuration files, license and readme
