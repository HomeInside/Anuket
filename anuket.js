/*!
 * Anuket, Awesome Google Maps utils using JS
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 3.0.c
 * http://jorgebrunal.xyz/projects/anuket/
 * Released under the MIT License.
 */

var Anuket = (function () {
	var fn = {};
	var AnuketMap = null;
	var iconfirst = '/images/clusters/userpin2.png';
	var version = '3.0.b';

	var cities = [
		{
			city: 'Maidenhead',
			lat: 51.5180094,
			lng: -0.7637034,
		},
	];

	var latlong = {};
	var theZoom = 12;
	var actualZoom = theZoom;
	var max_distance = 12.0;
	var initMarker = false;
	var useGeolocation = false;

	var markers;
	var clickOnMarker = null;

	/**
	 * Merge defaults with user options
	 * @private
	 * @param {Object} defaults Default settings
	 * @param {Object} options User options
	 * @returns {Object} Merged values of defaults and options
	 */
	var extend = function (defaults, options) {
		var extended = {};
		var prop;
		for (prop in defaults) {
			if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
				extended[prop] = defaults[prop];
			}
		}
		for (prop in options) {
			if (Object.prototype.hasOwnProperty.call(options, prop)) {
				extended[prop] = options[prop];
			}
		}
		return extended;
	};

	fn.streetView = function (coords, options) {
		console.log('Anuket streetView');
		var _opts = {
			container: '#gpanorama',
		};

		if (coords.constructor !== {}.constructor) {
			console.warn('streetView: coords is not a object');
			return false;
		}

		if (!coords.hasOwnProperty('lat') && !coords.hasOwnProperty('lng')) {
			console.warn('streetView: missing lat or lng attributes');
			return false;
		}

		if (options.constructor === {}.constructor) {
			var size = Object.keys(options).length;
			if (size > 0) {
				if (
					options.container !== undefined &&
					options.container !== null
				) {
					if (options.container !== '') {
						_opts.container = options.container;
					}
				}
			} else {
				console.warn('streetView: the options size is not correct');
				return false;
			}
		} else {
			console.warn('streetView: options is not a object');
			return false;
		}

		var panorama = GMaps.createPanorama({
			el: _opts.container,
			lat: coords.lat,
			lng: coords.lng,
		});

		return panorama;
	}; //streetView

	fn.setZoom = function (zoom) {
		return AnuketMap.setZoom(zoom);
	}; //setZoom

	fn.getZoom = function () {
		return AnuketMap.getZoom();
	}; //getZoom

	fn.getAnuketObj = function () {
		return AnuketMap;
	}; //getAnuketObj

	fn.getGoogleObj = function () {
		if (AnuketMap) {
			return AnuketMap.map;
		}
	}; //getGoogleObj

	fn.addMarker = function (latlong, options) {
		//fn.addMarker = function(latlong, drag, callback, container, firsticon, calbackDirecction) {
		console.log('adding marker', latlong);
		//var elicon = firsticon || null;
		var _opts = {
			title: 'Current Position',
			icon: null,
			draggable: false,
			infoWindow: {
				content:
					'<p>Current address:<br><strong>' +
					latlong.lat +
					', ' +
					latlong.lng +
					'</strong></p>',
			},
			info: null,
			onClick: null,
			onDragend: null,
		};

		if (options) {
			if (options.constructor === {}.constructor) {
				var size = Object.keys(options).length;
				if (size > 0) {
					if (options.title !== undefined) {
						if (options.title !== null) {
							_opts.title = options.title;
						}
					}

					if (options.icon !== undefined) {
						if (options.icon !== null) {
							_opts.icon = options.icon;
						}
					}

					if (options.draggable !== undefined) {
						if (options.draggable !== null) {
							if (options.draggable === true) {
								_opts.draggable = options.draggable;
							}
						}
					}

					if (options.infoWindow !== undefined) {
						if (options.infoWindow !== null) {
							_opts.infoWindow = options.infoWindow;
						}
					}

					if (options.info !== undefined) {
						if (options.info !== null) {
							_opts.info = options.info;
						}
					}

					if (options.onClick !== undefined) {
						if (options.onClick !== null) {
							if (typeof options.onClick === 'function') {
								_opts.onClick = options.onClick;
							}
						}
					}

					if (options.onDragend != undefined) {
						if (options.onDragend != null) {
							if (typeof options.onDragend == 'function') {
								_opts.onDragend = options.onDragend;
							}
						}
					}
				} else {
					console.warn('addMarker: the options size is not correct');
					return false;
				}
			} else {
				console.warn('addMarker: options is not a object');
				return false;
			}
		}
		console.log('addMarker _opts:...', _opts);
		marker = AnuketMap.addMarker({
			lat: latlong.lat,
			lng: latlong.lng,
			title: _opts.title,
			icon: _opts.icon,
			draggable: _opts.draggable,
			infoWindow: {
				content: _opts.infoWindow.content,
			},
			details: _opts.info,
			click: function (e) {
				if (typeof _opts.onClick === 'function') {
					//console.log('onClick addMarker...', e);
					_opts.onClick(e, { lat: latlong.lat, lng: latlong.lng });
				}
			},
		});

		if (_opts.draggable === true) {
			console.log('get dragend');

			google.maps.event.addListener(marker, 'dragend', function () {
				var pos = {};
				var getPosition = marker.getPosition();
				pos.lat = getPosition.lat();
				pos.lng = getPosition.lng();
				console.log('onDragend pos', pos);

				Anuket.inverseGeocoder({ lat: pos.lat, lng: pos.lng }, function (
					result,
				) {
					_opts.onDragend(result, pos);
				});
			});
		}
		return marker;
	}; //addMarkers

	fn.addGroupMarkers = function (markerList) {
		console.log('adding markerList', markerList);
		if (AnuketMap) {
			AnuketMap.addMarkers(markerList);
		} else {
			console.warning('addGroupMarkers: AnuketMap is missing');
		}
	}; //addGroupMarkers

	fn.getCurrentPosition = function (callback) {
		if (GMaps) {
			GMaps.geolocate({
				success: function (position) {
					if (AnuketMap) {
						var coords = {
							lat: position.coords.latitude,
							lng: position.coords.longitude,
						};
						AnuketMap.setCenter(coords.lat, coords.lng);
						AnuketMap.setZoom(theZoom);

						console.info('getCurrentPosition:', coords);

						if (typeof callback === 'function') {
							callback(coords, null);
						}
					} else {
						console.warn(
							'getCurrentPosition: AnuketObj is not defined',
						);
					}
				},
				error: function (error) {
					console.error('getCurrentPosition: Error code', error.code);
					console.error(
						'getCurrentPosition: Error message',
						error.message,
					);
					if (typeof callback === 'function') {
						callback(null, error);
					}
				},
				not_supported: function () {
					console.error(
						'getCurrentPosition: Your browser doesnt support Geolocation',
					);
					if (typeof callback === 'function') {
						callback(null, {
							error:
								'getCurrentPosition: Your browser doesnt support Geolocation',
						});
					}
				},
			});
		}
	}; //getCurrentPosition

	fn.run = function (_divMap, options) {
		console.info('Run:', _divMap, options);
		var _opts = {
			el: 'themap',
			controls: {
				mapType: false,
				overView: false,
				pan: true,
				position: google.maps.ControlPosition.RIGHT_CENTER,
				streetView: false,
				zoom: true,
			},
			geolocation: false,
			latlng: cities[0],
			marker: {
				icon: iconfirst,
				click: null,
			},
			zoom: theZoom,
		};

		if (_divMap === undefined || _divMap === null || _divMap === '') {
			console.warn('run: _divMap must be an String');
			return false;
		} else {
			_opts.el = _divMap;
		}

		// =======================================================================
		if (options) {
			if (options.constructor === {}.constructor) {
				var size = Object.keys(options).length;

				if (size > 0) {
					if (
						options.controls !== undefined &&
						options.controls !== null
					) {
						var controls = options.controls;

						if (controls.constructor != {}.constructor) {
							console.warn('run: controls must be an Object');
							return false;
						} else {
							// _opts.controls = controls;

							if (
								controls.mapType !== undefined &&
								controls.mapType !== null
							) {
								if (
									controls.mapType === true ||
									controls.mapType === false
								) {
									_opts.controls.mapType = controls.mapType;
								}
							}

							if (
								controls.overView !== undefined &&
								controls.overView !== null
							) {
								if (
									controls.overView === true ||
									controls.overView === false
								) {
									_opts.controls.overView = controls.overView;
								}
							}

							if (
								controls.pan !== undefined &&
								controls.pan !== null
							) {
								if (
									controls.pan === true ||
									controls.pan === false
								) {
									_opts.controls.pan = controls.pan;
								}
							}

							if (
								controls.position !== undefined &&
								controls.position !== null
							) {
								_opts.controls.position = controls.position;
							}

							if (
								controls.streetView !== undefined &&
								controls.streetView !== null
							) {
								if (
									controls.streetView === true ||
									controls.streetView === false
								) {
									_opts.controls.streetView =
										controls.streetView;
								}
							}

							if (
								controls.zoom !== undefined &&
								controls.zoom !== null
							) {
								if (
									controls.zoom === true ||
									controls.zoom === false
								) {
									_opts.controls.zoom = controls.zoom;
								}
							}
						}
					}

					if (
						options.geolocation !== undefined &&
						options.geolocation !== null
					) {
						if (options.geolocation === true) {
							_opts.geolocation = options.geolocation;
						}
					}

					if (
						options.latlng !== undefined &&
						options.latlng !== null
					) {
						var coords = options.latlng;

						if (coords.constructor != {}.constructor) {
							console.warn('run: latlng must be an Object');
							return false;
						} else {
							if (
								!coords.hasOwnProperty('lat') &&
								!coords.hasOwnProperty('lng')
							) {
								console.warn(
									'run: missing lat or lng attributes',
								);
								return false;
							} else {
								_opts.latlng = options.latlng;
							}
						}
					}

					if (
						options.marker !== undefined &&
						options.marker !== null
					) {
						var initMarker = options.marker;

						if (initMarker.constructor != {}.constructor) {
							console.warn('run: marker must be an Object');
							return false;
						} else {
							if (
								!initMarker.hasOwnProperty('icon') &&
								!initMarker.hasOwnProperty('click')
							) {
								console.warn(
									'run: missing icon or click attributes',
								);
								return false;
							} else {
								if (typeof initMarker.click === 'function') {
									_opts.marker.click = initMarker.click;
								}

								if (
									initMarker.icon !== undefined &&
									initMarker.icon !== null
								) {
									if (initMarker.icon !== '') {
										_opts.marker.icon = initMarker.icon;
									}
								}
							}
						}
					}

					if (options.zoom !== undefined && options.zoom !== null) {
						if (
							parseInt(options.zoom) >= 0 &&
							parseInt(options.zoom) <= 18
						) {
							_opts.zoom = options.zoom;
						}
					}
				} else {
					console.warn('run: The options size is not correct');
					return false;
				}
			} else {
				console.warn('run: options is not a object');
				return false;
			}
		} else {
			console.info('run: now using current configuration');
		}

		// if (AnuketMap) {
		AnuketMap = new GMaps({
			el: _opts.el,
			lat: _opts.latlng.lat,
			lng: _opts.latlng.lng,
			zoom: _opts.zoom,

			mapTypeControl: _opts.controls.mapType,
			overviewMapControl: _opts.controls.overView,

			panControl: _opts.controls.pan,
			streetViewControl: _opts.controls.streetView,

			zoomControl: _opts.controls.zoom,
			zoomControlOptions: {
				position: _opts.controls.position,
			},
		});

		AnuketMap.setCenter(_opts.latlng.lat, _opts.latlng.lng);

		if (_opts.geolocation === true) {
			fn.getCurrentPosition(function (coords) {
				if (_opts.marker.click !== null && _opts.marker.icon !== null) {
					fn.addMarker(
						coords,
						false,
						_opts.marker.click,
						null,
						_opts.marker.icon,
					);
				}
			});
		} else {
			if (_opts.marker.click !== null && _opts.marker.icon !== null) {
				fn.addMarker(
					_opts.latlng,
					false,
					_opts.marker.click,
					null,
					_opts.marker.icon,
				);
			}
		}
		// }else{
		// 	console.warn('AnuketMap fail', AnuketMap);
		// } //AnuketMap
	}; //run

	fn.removeMarkers = function () {
		if (AnuketMap) {
			AnuketMap.removeMarkers();
		}
	}; //removeMarkers

	fn.clearMap = function (options) {
		if (AnuketMap) {
			AnuketMap.removePolylines();
			AnuketMap.removeMarkers();
			AnuketMap.removePolygons();
			AnuketMap.removeOverlay();

			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (options.origin !== undefined) {
							if (options.origin === true) {
								fn.centerOverOrigin();
							}
						}

						if (options.marker !== undefined) {
							if (options.marker === true) {
								fn.addMarker(
									latlong,
									false,
									null,
									null,
									iconfirst,
								);
							}
						}
					} else {
						console.warn(
							'clearMap: the options size is not correct',
						);
						return false;
					}
				} else {
					console.warn('clearMap: options is not a object');
					return false;
				}
			}
		}
	}; //clearMap

	fn.centerMap = function (lat, long, options) {
		if (AnuketMap) {
			AnuketMap.setCenter(lat, long);

			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (
							options.zoom !== undefined &&
							options.zoom !== null
						) {
							if (options.zoom >= 0 && options.zoom <= 18) {
								AnuketMap.setZoom(options.zoom);
							}
						}
					} else {
						console.warn(
							'centerMap: the options size is not correct',
						);
						return false;
					}
				} else {
					console.warn('centerMap: options is not a object');
					return false;
				}
			}
		}
	}; //centerMap

	fn.drawPolygon = function (paths, options) {
		if (AnuketMap) {
			var _opts = {
				strokeColor: '#0E51DC',
				strokeOpacity: 1,
				strokeWeight: 2,
				fillColor: '#0E51DC',
				fillOpacity: 0.2,
				toRemove: null,
			};

			if (paths.constructor !== [].constructor) {
				console.warn('drawPolygon: paths must be an Array');
				return false;
			}

			if (paths.length <= 0) {
				console.warn('drawPolygon: paths must have at least item');
				return false;
			}

			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (options.strokeColor !== undefined) {
							if (options.strokeColor !== null) {
								if (options.strokeColor !== '') {
									_opts.strokeColor = options.strokeColor;
								}
							}
						}

						if (options.strokeOpacity !== undefined) {
							if (options.strokeOpacity !== null) {
								if (Number.isInteger(options.strokeOpacity)) {
									if (
										options.strokeOpacity >= 1 &&
										options.strokeOpacity <= 10
									) {
										_opts.strokeOpacity =
											options.strokeOpacity;
									}
								}
							}
						}

						if (options.strokeWeight !== undefined) {
							if (options.strokeWeight !== null) {
								if (Number.isInteger(options.strokeWeight)) {
									if (
										options.strokeWeight >= 1 &&
										options.strokeWeight <= 10
									) {
										_opts.strokeWeight =
											options.strokeWeight;
									}
								}
							}
						}

						if (options.fillColor !== undefined) {
							if (options.fillColor !== null) {
								if (options.fillColor !== '') {
									_opts.fillColor = options.fillColor;
								}
							}
						}

						if (options.fillOpacity !== undefined) {
							if (options.fillOpacity !== null) {
								if (
									parseFloat(options.fillOpacity) >= 0.0 &&
									parseFloat(options.fillOpacity) <= 1.0
								) {
									_opts.fillOpacity = options.fillOpacity;
								}
							}
						}

						if (options.toRemove !== undefined) {
							if (options.toRemove !== null) {
								if (options.toRemove !== '') {
									_opts.toRemove = options.toRemove;
								}
							}
						}
					} else {
						console.warn(
							'drawPolygon: the options size is not correct',
						);
						return false;
					}
				} else {
					console.warn('drawPolygon: options is not a object');
					return false;
				}
			}

			switch (_opts.toRemove) {
				case 'all':
					fn.clearMap();
					break;
				case 'polygons':
					AnuketMap.removePolygons();
					break;
				case 'polylines':
					AnuketMap.removePolylines();
					break;
				default:
					break;
			}

			var polygon = AnuketMap.drawPolygon({
				paths: paths,
				strokeColor: _opts.strokeColor,
				strokeOpacity: _opts.strokeOpacity,
				strokeWeight: _opts.strokeWeight,
				fillColor: _opts.fillColor,
				fillOpacity: _opts.fillOpacity,
			});

			return polygon;
		} else {
			console.warn('drawPolygon: AnuketObj is not defined');
			return false;
		}
	}; //drawPolygon

	fn.drawPolyline = function (paths, options) {
		if (AnuketMap) {
			var _opts = {
				strokeColor: '#0E51DC',
				strokeOpacity: 1,
				strokeWeight: 2,
				toRemove: null,
			};

			if (paths.constructor !== [].constructor) {
				console.warn('drawPolyline: paths must be an Array');
				return false;
			}

			if (paths.length <= 0) {
				console.warn('drawPolyline: paths must have at least item');
				return false;
			}

			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (options.strokeColor !== undefined) {
							if (options.strokeColor !== null) {
								if (options.strokeColor !== '') {
									_opts.strokeColor = options.strokeColor;
								}
							}
						}

						if (options.strokeOpacity !== undefined) {
							if (options.strokeOpacity !== null) {
								if (Number.isInteger(options.strokeOpacity)) {
									if (
										options.strokeOpacity >= 1 &&
										options.strokeOpacity <= 10
									) {
										_opts.strokeOpacity =
											options.strokeOpacity;
									}
								}
							}
						}

						if (options.strokeWeight !== undefined) {
							if (options.strokeWeight !== null) {
								if (Number.isInteger(options.strokeWeight)) {
									if (
										options.strokeWeight >= 1 &&
										options.strokeWeight <= 10
									) {
										_opts.strokeWeight =
											options.strokeWeight;
									}
								}
							}
						}

						if (options.toRemove !== undefined) {
							if (options.toRemove !== null) {
								if (options.toRemove !== '') {
									_opts.toRemove = options.toRemove;
								}
							}
						}
					} else {
						console.warn(
							'drawPolyline: the options size is not correct',
						);
						return false;
					}
				} else {
					console.warn('drawPolyline: options is not a object');
					return false;
				}
			}

			switch (_opts.toRemove) {
				case 'all':
					fn.clearMap();
					break;
				case 'polygons':
					AnuketMap.removePolygons();
					break;
				case 'polylines':
					AnuketMap.removePolylines();
					break;
				default:
					break;
			}

			var polyLine = AnuketMap.drawPolyline({
				path: paths,
				strokeColor: _opts.strokeColor,
				strokeOpacity: _opts.strokeOpacity,
				strokeWeight: _opts.strokeWeight,
			});

			return polyLine;
		} else {
			console.warn('drawPolyline: AnuketObj is not defined');
			return false;
		}
	}; //drawPolyline

	// to fix
	fn.drawCircle = function (coords, radius, options) {
		if (AnuketMap) {
			var _opts = {
				radius: 100,
				strokeColor: '#FF0000',
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: '#FF0000',
				fillOpacity: 0.35,
				toRemove: null,
				draggable: false,
				editable: false,
			};

			if (coords.constructor !== {}.constructor) {
				console.warn('drawCircle: coords must be an Object');
				return false;
			}

			var size = Object.keys(coords).length;
			if (size <= 0) {
				console.warn('drawCircle: the coords size is not correct');
				return false;
			}

			if (radius === undefined || radius === null) {
				console.warn('drawCircle: The radius is required');
				return false;
			}

			if (Number.isInteger(radius)) {
				if (radius >= 1 && radius <= 10000) {
					_opts.radius = radius;
				} else {
					console.warn(
						'drawCircle: The radius size should be between 1 and 10000 meters',
					);
					return false;
				}
			} else {
				console.warn('drawCircle: The radius is not a number');
				return false;
			}

			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (options.strokeColor !== undefined) {
							if (options.strokeColor !== null) {
								if (options.strokeColor !== '') {
									_opts.strokeColor = options.strokeColor;
								}
							}
						}

						if (options.strokeOpacity !== undefined) {
							if (options.strokeOpacity !== null) {
								if (Number.isInteger(options.strokeOpacity)) {
									if (
										options.strokeOpacity >= 1 &&
										options.strokeOpacity <= 10
									) {
										_opts.strokeOpacity =
											options.strokeOpacity;
									}
								}
							}
						}

						if (options.strokeWeight !== undefined) {
							if (options.strokeWeight !== null) {
								if (Number.isInteger(options.strokeWeight)) {
									if (
										options.strokeWeight >= 1 &&
										options.strokeWeight <= 10
									) {
										_opts.strokeWeight =
											options.strokeWeight;
									}
								}
							}
						}

						if (options.fillColor !== undefined) {
							if (options.fillColor !== null) {
								if (options.fillColor !== '') {
									_opts.fillColor = options.fillColor;
								}
							}
						}

						if (options.fillOpacity !== undefined) {
							if (options.fillOpacity !== null) {
								if (
									parseFloat(options.fillOpacity) >= 0.0 &&
									parseFloat(options.fillOpacity) <= 1.0
								) {
									_opts.fillOpacity = options.fillOpacity;
								}
							}
						}

						if (options.toRemove !== undefined) {
							if (options.toRemove !== null) {
								if (options.toRemove !== '') {
									_opts.toRemove = options.toRemove;
								}
							}
						}

						if (options.editable !== undefined) {
							if (options.editable !== null) {
								if (options.editable === true) {
									_opts.editable = options.editable;
								}
							}
						}
						if (options.draggable !== undefined) {
							if (options.draggable !== null) {
								if (options.draggable === true) {
									_opts.draggable = options.draggable;
								}
							}
						}
					} else {
						console.warn(
							'drawCircle: the options size is not correct',
						);
						return false;
					}
				} else {
					console.warn('drawCircle: options is not a object');
					return false;
				}
			}

			switch (_opts.toRemove) {
				case 'all':
					fn.clearMap();
					break;
				case 'polygons':
					AnuketMap.removePolygons();
					break;
				case 'polylines':
					AnuketMap.removePolylines();
					break;
				default:
					break;
			}

			console.warn('drawCircle: _opts', _opts);

			var circleOptions = {
				lat: coords.lat,
				lng: coords.lng,
				radius: _opts.radius,
				strokeColor: _opts.strokeColor,
				strokeOpacity: _opts.strokeOpacity,
				strokeWeight: _opts.strokeWeight,
				fillColor: _opts.fillColor,
				fillOpacity: _opts.fillOpacity,
			};

			/*
			if (_opts.editable === true) {
				circleOptions = extend(circleOptions, {
					editable: _opts.editable,
				});
			}

			if (_opts.draggable === true) {
				circleOptions = extend(circleOptions, {
					draggable: _opts.draggable,
				});
			}
			*/

			console.warn('drawCircle: circleOptions', circleOptions);

			var circle = AnuketMap.drawCircle(circleOptions);

			return circle;
		} else {
			console.warn('drawCircle: AnuketObj is not defined');
			return false;
		}
	}; //drawCircle

	fn.fitBounds = function (shape, type) {
		console.warn('fitBounds fn:', type);
		if (AnuketMap) {
			if (shape === undefined || shape === null) {
				console.warn(
					'fitBounds: Shape must be a Marker, a Circle or a Polygon Object.',
				);
				return false;
			}

			if (type === undefined || type === null) {
				console.warn(
					'fitBounds: type missing option [marker, circle, polygon].',
				);
				return false;
			}

			switch (type) {
				case 'marker':
					if (shape.getVisible()) {
						var bounds = new google.maps.LatLngBounds();
						bounds.extend(shape.getPosition());
						AnuketMap.fitBounds(bounds);
					} else {
						console.warn('fitBounds: shape is not visible');
					}
					break;
				case 'circle':
					var bounds = shape.getBounds();
					AnuketMap.fitBounds(bounds);
					break;
				case 'polygon':
					var bounds = new google.maps.LatLngBounds();
					var polyPaths = shape.getPaths();
					var path;
					for (var i = 0; i < polyPaths.getLength(); i++) {
						path = polyPaths.getAt(i);
						for (var ii = 0; ii < path.getLength(); ii++) {
							bounds.extend(path.getAt(ii));
						}
					}
					AnuketMap.fitBounds(bounds);
					break;
				case 'polyline':
					var bounds = new google.maps.LatLngBounds();
					var linePaths = shape.getPath().getArray();
					for (var i = 0; i < linePaths.length; i++) {
						bounds.extend(linePaths[i]);
					}
					AnuketMap.fitBounds(bounds);
					break;
				default:
					console.warn('fitBounds: unknown option', type);
					break;
			}
		} else {
			console.warn('fitBounds: AnuketObj is not defined');
			return false;
		}
	}; //fitBounds

	fn.centerOverOrigin = function (options) {
		var origin = cities[0];

		AnuketMap.setCenter(origin.lat, origin.lng);
		if (options.zoom) {
			AnuketMap.setZoom(options.zoom);
		}
	}; //centerOverOrigin

	fn.getOriginPosition = function () {
		return cities[0];
	}; //getOriginPosition

	fn.drawRoute = function (origin, destiny, options) {
		var _opts = {
			markers: false,
			strokeColor: '#0E51DC',
			strokeOpacity: 0.8,
			strokeWeight: 7,
			travelMode: 'driving',
		};

		if (origin.constructor != [].constructor) {
			console.warn('drawRoute: origin must be an Array');
			return false;
		}

		if (destiny.constructor != [].constructor) {
			console.warn('drawRoute: destiny must be an Array');
			return false;
		}

		if (options) {
			if (options.constructor === {}.constructor) {
				var size = Object.keys(options).length;

				if (size > 0) {
					if (options.markers !== undefined) {
						if (options.markers !== null) {
							if (options.markers === true) {
								_opts.markers = options.markers;
							}
						}
					}

					if (options.strokeColor !== undefined) {
						if (options.strokeColor !== null) {
							if (options.strokeColor !== '') {
								_opts.strokeColor = options.strokeColor;
							}
						}
					}

					if (options.strokeOpacity !== undefined) {
						if (options.strokeOpacity !== null) {
							if (
								parseFloat(options.strokeOpacity) >= 0.0 &&
								parseFloat(options.strokeOpacity) <= 1.0
							) {
								_opts.strokeOpacity = options.strokeOpacity;
							}
						}
					}

					if (options.strokeWeight !== undefined) {
						if (options.strokeWeight !== null) {
							if (Number.isInteger(options.strokeWeight)) {
								if (
									options.strokeWeight >= 1 &&
									options.strokeWeight <= 10
								) {
									_opts.strokeWeight = options.strokeWeight;
								}
							}
						}
					}

					if (options.travelMode !== undefined) {
						if (options.travelMode !== null) {
							if (options.travelMode !== '') {
								_opts.travelMode = options.travelMode;
							}
						}
					}
				} else {
					console.warn('drawRoute: the options size is not correct');
					return false;
				}
			} else {
				console.warn('drawRoute: options is not a object');
				return false;
			}
		}

		if (origin.length > 0 && destiny.length > 0) {
			console.log('drawRoute:', origin, destiny);
			if (AnuketMap) {
				AnuketMap.removePolylines();

				AnuketMap.drawRoute({
					origin: origin,
					destination: destiny,
					strokeColor: _opts.strokeColor,
					strokeOpacity: _opts.strokeOpacity,
					strokeWeight: _opts.strokeWeight,
					travelMode: _opts.travelMode,
				});

				if (_opts.markers === true) {
					var originPos = { lat: origin[0], lng: origin[1] };
					var destinyPos = { lat: destiny[0], lng: destiny[1] };

					fn.addMarker(originPos, false, null);
					fn.addMarker(destinyPos, false, null);
				}
			} else {
				console.warn('drawRoute: AnuketObj is not defined');
			}
		} else {
			console.warn(
				'drawRoute: origin or destiny must have at least item',
			);
			return false;
		}
	}; //drawRoute

	fn.removeLayer = function (layer) {
		if (AnuketMap) {
			AnuketMap.removeLayer(layer);
		}
	}; //removeLayer

	// to fix
	fn.removeFeature = function (feat) {
		if (AnuketMap) {
			var goo = AnuketMap.map;
			goo.data.forEach(function (feature) {
				console.log('feature', feature);

				if (feature.getProperty('letter') == 'G') {
					goo.data.remove(feature);
				}
			});

			if (feat !== null) {
				feat.setMap(null);
			}
		}
	}; //removeFeature

	fn.setLayer = function (layer) {
		if (AnuketMap) {
			AnuketMap.addLayer(layer);
		}
	}; //setLayer

	fn.setMapType = function (el) {
		console.info('setMapType fn', el);
		switch (el) {
			case 'hybrid':
			case 'roadmap':
			case 'satellite':
			case 'terrain':
				AnuketMap.setStyle(el);
				break;
			default:
				console.warn('setMapType: unknow option', el);
				AnuketMap.setStyle('roadmap');
				break;
		}
	}; //setMapType

	fn.geocoder = function (address, callBack, options) {
		console.log('Anuket geocoder');
		var _opts = {
			callBack: null,
			centerMap: false,
			country: null,
			marker: null,
			zoom: 8,
		};

		if (address === undefined || address === null) {
			console.warn('geocoder: address must be an String');
			return false;
		}

		if (typeof callBack === 'function') {
			_opts.callBack = callBack;
		} else {
			console.warn('geocoder: callback is not a valid function');
			return false;
		}

		if (options) {
			if (options.constructor === {}.constructor) {
				var size = Object.keys(options).length;

				if (size > 0) {
					if (options.centerMap !== undefined) {
						if (options.centerMap !== null) {
							if (options.centerMap === true) {
								_opts.centerMap = options.centerMap;
							}
						}
					}

					if (options.country !== undefined) {
						if (options.country !== null) {
							if (options.country !== '') {
								_opts.country = options.country;
							}
						}
					}

					if (options.marker !== undefined) {
						if (options.marker !== null) {
							if (options.marker === true) {
								_opts.marker = options.marker;
							}
						}
					}

					if (options.zoom !== undefined) {
						if (options.zoom !== null) {
							if (
								parseInt(options.zoom) >= 0 &&
								parseInt(options.zoom) <= 18
							) {
								_opts.zoom = options.zoom;
							}
						}
					}
				} else {
					console.warn('geocoder: The options size is not correct');
					return false;
				}
			} else {
				console.warn('geocoder: options is not a object');
				return false;
			}
		} else {
			console.log('geocoder: now using current configuration');
		}

		var geocoderOptions = {
			address: address,
			callback: function (results, status) {
				if (status === 'OK') {
					var latlng = results[0].geometry.location;

					if (_opts.centerMap === true) {
						if (AnuketMap) {
							AnuketMap.setCenter(latlng.lat(), latlng.lng());
						}

						if (_opts.zoom >= 0) {
							if (AnuketMap) {
								AnuketMap.setZoom(_opts.zoom);
							}
						}
					}

					if (_opts.marker === true) {
						// fn.addMarker = function(latlong, drag, callback, container, firsticon)
						if (AnuketMap) {
							fn.addMarker(
								{
									lat: latlng.lat(),
									lng: latlng.lng(),
								},
								false,
								null,
								null,
								null,
							);
						}
					}

					if (_opts.callBack !== null) {
						_opts.callBack(
							{ lat: latlng.lat(), lng: latlng.lng() },
							results,
							status,
						);
					}
				} else {
					console.warn(
						'geocoder was not successful for the following reason:',
						status,
					);
					if (_opts.callBack !== null) {
						_opts.callBack({}, {}, status);
					}
				}
			},
		};

		if (_opts.country != null) {
			extend(geocoderOptions, {
				componentRestrictions: {
					country: _opts.country,
				},
			});
		}

		GMaps.geocode(geocoderOptions);
	}; // geocoder

	fn.inverseGeocoder = function (coords, callBack, options) {
		console.log('Anuket inverseGeocoder');
		var _opts = {
			callBack: null,
			centerMap: false,
			marker: null,
			zoom: 8,
		};

		if (coords === undefined || coords === null) {
			console.warn('inverseGeocoder: missing parameter coords');
			return false;
		}
		if (coords.constructor != {}.constructor) {
			console.warn('inverseGeocoder: coords must be an Object');
			return false;
		}

		if (!coords.hasOwnProperty('lat') && !coords.hasOwnProperty('lng')) {
			console.warn('inverseGeocoder: missing lat or lng attributes');
			return false;
		}

		if (typeof callBack === 'function') {
			_opts.callBack = callBack;
		} else {
			console.warn('inverseGeocoder: callback is not a valid function');
			return false;
		}

		if (options) {
			if (options.constructor === {}.constructor) {
				var size = Object.keys(options).length;

				if (size > 0) {
					if (options.centerMap !== undefined) {
						if (options.centerMap !== null) {
							if (options.centerMap === true) {
								_opts.centerMap = options.centerMap;
							}
						}
					}

					if (options.marker !== undefined) {
						if (options.marker !== null) {
							if (options.marker === true) {
								_opts.marker = options.marker;
							}
						}
					}

					if (options.zoom !== undefined) {
						if (options.zoom !== null) {
							if (
								parseInt(options.zoom) >= 0 &&
								parseInt(options.zoom) <= 18
							) {
								_opts.zoom = options.zoom;
							}
						}
					}
				} else {
					console.warn(
						'inverseGeocoder: The options size is not correct',
					);
					return false;
				}
			} else {
				console.warn('inverseGeocoder: options is not a object');
				return false;
			}
		} else {
			console.log('inverseGeocoder: now using current configuration');
		}

		var inverseOptions = {
			location: coords,
			callback: function (results, status) {
				if (status === 'OK') {
					var formatted_address = results[0].formatted_address;

					if (_opts.centerMap === true) {
						if (AnuketMap) {
							AnuketMap.setCenter(coords.lat, coords.lng);
						}

						if (_opts.zoom >= 0) {
							if (AnuketMap) {
								AnuketMap.setZoom(_opts.zoom);
							}
						}
					}

					if (_opts.marker === true) {
						// fn.addMarker = function(latlong, drag, callback, container, firsticon)
						if (AnuketMap) {
							fn.addMarker(coords, false, null, null, null);
						}
					}

					if (_opts.callBack !== null) {
						_opts.callBack(formatted_address, results, status);
					}
				} else {
					console.warn(
						'inverseGeocoder was not successful for the following reason:',
						status,
					);
					if (_opts.callBack !== null) {
						_opts.callBack('', {}, status);
					}
				}
			},
		};

		GMaps.geocode(inverseOptions);
	}; // inverseGeocoder

	fn.version = function () {
		console.info('Anuket version:', version);
	}; // version

	return fn;
})(); //Anuket
