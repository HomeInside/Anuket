module.exports = {
	root: true,
	env: {
		node: true,
		browser: true,
		es6: true,
	},
	parserOptions: {
		ecmaVersion: 7,
		sourceType: 'module',
		allowImportExportEverywhere: true,
		ecmaFeatures: {
			modules: true,
			jsx: true,
		},
		parser: 'babel-eslint',
	},
	overrides: [
		{
			files: ['*.vue'],
			rules: {
				'prettier/prettier': [
					'vue',
					{
						parser: 'vue',
					},
				],
			},
		},
	],
	plugins: ['prettier'],
	extends: ['eslint:recommended', 'prettier'],

	settings: {
		'import/resolver': {
			node: {
				extensions: ['.js', '.vue'],
			},
		},
	},
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-tabs': 0,
		'arrow-parens': 0,
		'generator-star-spacing': ['error', { before: true, after: false }],
		quotes: [
			2,
			'single',
			{
				allowTemplateLiterals: true,
				avoidEscape: true,
			},
		],
		'arrow-parens': 0,
		'generator-star-spacing': 2,
		indent: [2, 'tab', { SwitchCase: 1 }],
		'linebreak-style': ['error', 'unix'],
		semi: ['error', 'always'],
		'import/extensions': [
			'error',
			'always',
			{
				js: 'never',
				vue: 'never',
			},
		],
		semi: [2, 'always'],
		eqeqeq: [2, 'always'],
		strict: [2, 'global'],
		'key-spacing': [
			2,
			{
				afterColon: true,
			},
		],
		'prettier/prettier': 'error',

		'object-shorthand': 0,
		'no-empty': 0,
		'no-unused-vars': 0,
		'no-constant-condition': 0,
		'no-undef': 0,
		'no-trailing-spaces': 0,
		'no-underscore-dangle': 0,

		curly: ['error', 'all'],
		'comma-dangle': ['error', 'only-multiline'],
		camelcase: ['error', { properties: 'never' }],
		'object-curly-spacing': ['error', 'always'],
		'space-before-function-paren': ['error', 'never'],
		'space-before-blocks': ['error', 'always'],
		'space-infix-ops': ['error', { int32Hint: false }],
		'no-alert': 'error',
		'no-dupe-args': 'error',
		'no-duplicate-case': 'error',
		'no-duplicate-imports': 'error',
		'no-empty': 'error',
	},
};
