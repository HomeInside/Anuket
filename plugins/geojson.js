/*!
 * GeoJson plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.a or greater
 * https://jorgebrunal.xyz/projects/anuket
 * Released under the MIT License.
 */

// example
//https://developers.google.com/maps/documentation/javascript/datalayer
//
//GeoJson layer: https://storage.googleapis.com/mapsdevsite/json/google.json
//
var GeoJsonPlugin = (function(plugin) {

	// var anuketObj = plugin.getObjectMap();
	var anuketObj = plugin.getAnuketObj();
	var gooMap = plugin.getGoogleObj();

	plugin.geoJson = function(url, options) {
		console.log('Anuket GeoJson');
		var _opts = {
			'callback': null,
			'centerMap': {
				'lat': null,
				'lng': null
			},
			'url': url,
			'zoom': 8
		};

		if (anuketObj) {
			if ((url !== undefined) && (url !== null)) {
				if (url !== '') {
					_opts.url = url;
				}
			}else{
				console.warn('GeoJson: missing parameter url');
				return false;
			}

			if (options) {
				if (typeof(options.callback) === 'function') {
					_opts.callback = options.callBack;
				} else {
					console.warn('GeoJson: callback is not a valid function');
					return false;
				}


				if ((options.zoom !== undefined) && (options.zoom !== null)) {
					if ((parseInt(options.zoom) >= 0) && (parseInt(options.zoom) <= 18)) {
						_opts.zoom = options.zoom;
					}
				}
			}

			geo = new google.maps.Data();
			geo.loadGeoJson(_opts.url);

			// create some layer control logic for turning on data1
			geo.setMap(gooMap) // or restyle or whatever

			// turn off data1 and turn on data2
			// geo.setMap(null) // hides it
			// geo = gooMap.data.loadGeoJson(_opts.url);
			console.warn('GeoJson: data', geo);

			// anuketObj.setCenter(_opts.lat, _opts.lng);

			anuketObj.setZoom(_opts.zoom);
			return geo;

		} //anuketObj
	} // GeoJson

	return plugin;

})(Anuket || {});
