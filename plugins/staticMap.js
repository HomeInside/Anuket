/*!
 * static Map plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.b or greater
 * https://jorgebrunal.co/projects/anuket
 * Released under the MIT License.
 */

// example
// https://developers.google.com/maps/documentation/maps-static/dev-guide

var staticMapPlugin = (function (plugin) {

	plugin.staticMap = function (lat, lng, options) {
		console.log('Anuket staticMap');
		var _opts = {
			'url': 'https://maps.googleapis.com/maps/api/staticmap',
			'center': null,
			'zoom': 16,
			'size': '600x400',
			'maptype': 'roadmap',
			'markers': 'color:green|label:S|',
			'format': 'png',
			'scale': 1,
			'key': null,
			'callback': null,
		};

		if (lat === undefined || lat === null) {
			console.warn('staticMap: lat must be an Float');
			return false;
		}

		if (lng === undefined || lng === null) {
			console.warn('staticMap: lng must be an Float');
			return false;
		}

		if (options) {
			if (typeof (options.callback) === 'function') {
				_opts.callback = options.callback;
			} else {
				console.warn('staticMap: callback is not a valid function');
				return false;
			}

			if ((options.key !== undefined) && (options.key !== null)) {
				if (options.key !== '') {
					_opts.key = options.key;
				}
			} else {
				console.warn('staticMap: missing parameter key');
				return false;
			}

			if ((options.size !== undefined) && (options.size !== null)) {
				if (options.size !== '') {
					_opts.size = options.size;
				}
			} else {
				console.warn('staticMap: missing parameter size');
				return false;
			}

			if ((options.zoom !== undefined) && (options.zoom !== null)) {
				if ((parseInt(options.zoom) >= 0) && (parseInt(options.zoom) <= 18)) {
					_opts.zoom = options.zoom;
				}
			}
		}

		var Url = `${_opts.url}?zoom=${_opts.zoom}&size=${_opts.size}&maptype=${_opts.maptype}&markers=${_opts.markers}${lat},${lng}&format=${_opts.format}&scale=${_opts.scale}&key=${_opts.key}`;

		fetch(Url)
			.then(function (response) {
				console.log("staticMap response:", response);
				if (response.ok) {
					response.blob()
						.then(function (imgBlob) {
							var objectURL = URL.createObjectURL(imgBlob);
							console.log("staticMap objectURL:", objectURL);
							_opts.callback({ 'error': false, 'message': null, 'imageBlob': objectURL });
						});
				} else {
					var message = 'staticMap: Network response OK, but HTTP response not OK';
					console.warn(message);
					_opts.callback({ 'error': true, 'message': message, 'imageBlob': null });
				}
			}).catch(function (error) {
				console.error('staticMap: There was a problem with the Fetch request:', error.message);
				_opts.callback({ 'error': true, 'message': error.message, 'imageBlob': null });
			});
	} // staticMap

	return plugin;

})(Anuket || {});
