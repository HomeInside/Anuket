/*!
 * OpenStreet Map plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.a or greater
 * https://jorgebrunal.co/projects/anuket
 * Released under the MIT License.
 */

var osmMapPlugin = (function(plugin) {

	var anuketObj = plugin.getObjectMap();

	plugin.osmMap = function(options) {
		console.log('Anuket osmMap');
		var _opts = {
			zoom: 8
		};

		if (anuketObj) {
			if (options) {
				if ((options.zoom !== undefined) && (options.zoom !== null)) {
					if ((parseInt(options.zoom) >= 0) && (parseInt(options.zoom) <= 18)) {
						_opts.zoom = options.zoom;
					}
				}
			}

			anuketObj.addMapType("osm", {
				getTileUrl: function(coord, zoom) {
					return "https://a.tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
				},
				tileSize: new google.maps.Size(256, 256),
				name: "OpenStreetMap",
				maxZoom: 18,
				minZoom: 0
			});

			anuketObj.setStyle('osm');

			anuketObj.setZoom(_opts.zoom);

		} //anuketObj
	} // osmMap

	return plugin;

})(Anuket || {});
