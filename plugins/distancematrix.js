/*!
 * Distance matrix plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.a or greater
 * https://jorgebrunal.xyz/projects/anuket
 * Released under the MIT License.
 */

// example
// https://developers.google.com/maps/documentation/distance-matrix/start
// https://developers.google.com/maps/documentation/javascript/distancematrix

var distanceMatrixPlugin = (function(plugin) {
	var anuketObj = plugin.getObjectMap();

	plugin.distanceMatrix = function(origins, destinations, options) {
		console.log('Anuket distanceMatrix');
		var _opts = {
			callback: null,
			travelMode: 'DRIVING',
			transitOptions: null,
			drivingOptions: null,
			unitSystem: null,
			avoidHighways: false,
			avoidTolls: false,
		};

		if (anuketObj) {
			if (options) {
				if (typeof options.callback === 'function') {
					_opts.callback = options.callBack;
				} else {
					console.warn('distanceMatrix: callback is not a valid function');
					return false;
				}

				if (options.travelMode !== undefined && options.travelMode !== null) {
					if (options.travelMode !== '') {
						_opts.travelMode = options.travelMode;
					}
				} else {
					console.warn('distanceMatrix: missing parameter travelMode');
					return false;
				}
			}

			// var origin1 = new google.maps.LatLng(55.930385, -3.118425);
			// var origin2 = 'Greenwich, England';
			// var destinationA = 'Stockholm, Sweden';
			// var destinationB = new google.maps.LatLng(50.087692, 14.421150);

			var service = new google.maps.DistanceMatrixService();
			service.getDistanceMatrix(
				{
					// origins: [origin1, origin2],
					origins: [origins],
					// destinations: [destinationA, destinationB],
					destinations: [destinations],
					travelMode: _opts.travelMode,
					// transitOptions: TransitOptions,
					// drivingOptions: DrivingOptions,
					// unitSystem: UnitSystem,
					avoidHighways: _opts.avoidHighways,
					avoidTolls: _opts.avoidTolls,
				},
				serviceCallback,
			);

			function serviceCallback(response, status) {
				if (status == 'OK') {
					var origins = response.originAddresses;
					var destinations = response.destinationAddresses;

					for (var i = 0; i < origins.length; i++) {
						var results = response.rows[i].elements;
						for (var j = 0; j < results.length; j++) {
							var element = results[j];
							var distance = element.distance.text;
							var duration = element.duration.text;
							var from = origins[i];
							var to = destinations[j];
						}
					}
				}
			}
		} //anuketObj
	}; // distanceMatrix

	return plugin;
})(Anuket || {});
