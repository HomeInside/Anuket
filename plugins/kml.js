/*!
 * KML plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.a or greater
 * https://jorgebrunal.xyz/projects/anuket
 * Released under the MIT License.
 */

// example
//http://hpneo.github.io/gmaps/examples/kml.html
//https://developers.google.com/maps/documentation/javascript/kmllayer
//https://developers.google.com/maps/documentation/javascript/kml
//
//kml layer: http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss
//
var kmlPlugin = (function(plugin) {

	var anuketObj = plugin.getObjectMap();

	plugin.kml = function(options) {
		console.log('Anuket kml');
		var _opts = {
			'callback': null,
			'centerMap': {
				'lat': null,
				'lng': null
			},
			'url': 'http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss',
			'zoom': 8
		};

		if (anuketObj) {
			if (options) {
				if (typeof(options.callback) === 'function') {
					_opts.callback = options.callBack;
				} else {
					console.warn('kml: callback is not a valid function');
					return false;
				}

				if ((options.url !== undefined) && (options.url !== null)) {
					if (options.url !== '') {
						_opts.url = options.url;
					}
				}else{
					console.warn('kml: missing parameter url');
					return false;
				}

				if ((options.zoom !== undefined) && (options.zoom !== null)) {
					if ((parseInt(options.zoom) >= 0) && (parseInt(options.zoom) <= 18)) {
						_opts.zoom = options.zoom;
					}
				}
			}

			anuketObj.loadFromKML({
				url: _opts.url,
				suppressInfoWindows: true,
				events: {
					click: function(point){
						if (_opts.callback !== null) {
							_opts.callback(point);
						}
						infoWindow.setContent(point.featureData.infoWindowHtml);
						infoWindow.setPosition(point.latLng);
						infoWindow.open(anuketObj.map);
					}
				}
			});

			anuketObj.setCenter(_opts.lat, _opts.lng);

			anuketObj.setZoom(_opts.zoom);

		} //anuketObj
	} // kml

	return plugin;

})(Anuket || {});
