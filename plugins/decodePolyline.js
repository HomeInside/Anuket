/*!
 * decodePolyline plugin for Anuket
 * Author: Jorge Brunal Perez (aka Diniremix)
 * Version: 0.1
 * Require Anuket 3.0.a or greater
 * https://jorgebrunal.xyz/projects/anuket
 * Released under the MIT License.
 */

// example
// http://jsfiddle.net/upsidown/g391fmaw/
// https://developers.google.com/maps/documentation/javascript/examples/polyline-simple
// https://developers.google.com/maps/documentation/javascript/shapes#polylines
// https://developers.google.com/maps/documentation/javascript/geometry

var decodePolylinePlugin = (function(plugin) {
	var anuketObj = plugin.getAnuketObj();

	plugin.decodePolyline = function(encodedPoly, options) {
		console.log('Anuket decodePolyline');
		var _opts = {
			strokeColor: '#131540',
			strokeOpacity: 0.6,
			strokeWeight: 6,
			zIndex: 3,
		};

		if (encodedPoly === undefined || encodedPoly === null || encodedPoly === '') {
			console.warn('decodePolyline: missing parameter encodedPoly');
			return false;
		}

		if (google.maps.geometry === undefined || google.maps.geometry === null || google.maps.geometry === '') {
			console.warn('decodePolyline: the geometry library must be enabled');
			console.warn('visit https://developers.google.com/maps/documentation/javascript/geometry');
			return false;
		}

		if (anuketObj) {
			if (options) {
				if (options.constructor === {}.constructor) {
					var size = Object.keys(options).length;
					if (size > 0) {
						if (options.strokeColor !== undefined) {
							if (options.strokeColor !== null) {
								if (options.strokeColor !== '') {
									_opts.strokeColor = options.strokeColor;
								}
							}
						}

						if (options.strokeOpacity !== undefined) {
							if (options.strokeOpacity !== null) {
								if (Number.isInteger(options.strokeOpacity)) {
									if (options.strokeOpacity >= 1 && options.strokeOpacity <= 10) {
										_opts.strokeOpacity = options.strokeOpacity;
									}
								}
							}
						}

						if (options.strokeWeight !== undefined) {
							if (options.strokeWeight !== null) {
								if (Number.isInteger(options.strokeWeight)) {
									if (options.strokeWeight >= 1 && options.strokeWeight <= 10) {
										_opts.strokeWeight = options.strokeWeight;
									}
								}
							}
						}

						if (options.zIndex !== undefined) {
							if (options.zIndex !== null) {
								if (Number.isInteger(options.zIndex)) {
									if (options.zIndex >= 1 && options.zIndex <= 10) {
										_opts.zIndex = options.zIndex;
									}
								}
							}
						}
					} else {
						console.warn('decodePolyline: the options size is not correct');
						return false;
					}
				} else {
					console.warn('decodePolyline: options is not a object');
					return false;
				}
			}

			var decodePoly = google.maps.geometry.encoding.decodePath(encodedPoly);

			anuketObj.drawPolyline({
				path: decodePoly,
				strokeColor: _opts.strokeColor,
				strokeOpacity: _opts.strokeOpacity,
				strokeWeight: _opts.strokeWeight,
				zIndex: _opts.zIndex,
			});
		} //anuketObj
	}; // decodePolyline

	return plugin;
})(Anuket || {});
