# Anuket

Awesome Google Maps utils using JS


## Versión estable

Puedes descargar la versión estable del proyecto [por aquí](https://gitlab.com/HomeInside/Anuket), ó revisa los lanzamientos mas recientes, [por aquí](https://gitlab.com/HomeInside/Anuket/releases).


## Documentación

Revisa la documentación de la versión estable de **Anuket**, [por aquí](http://jorgebrunal.xyz/projects/anuket).


## Listado de Cambios

El listado completo de cambios (changelog), [por aquí](CHANGELOG.md).


## Instalación de este repositorio

Clonar este repositorio y alojarlo en una carpeta conveniente.

    git clone https://gitlab.com/HomeInside/Anuket.git


### Instalar dependencias del proyecto

```sh
$ npm install
```


### Construir el proyecto

```sh
$ npm run build
```


# Como Contribuir

Recomendamos leer la [guía de estilo de Javascript de Airbnb](https://github.com/airbnb/javascript) y revisar el apartado de [Referencias](/docs/menu/references.html#referencias)

Tú colaboración es muy valiosa, agradecemos que dediques algo de tu tiempo en las siguientes actividades:

- Crear [nuevas funcionalidades](/docs/modules/methods.html#metodos-publicos) ó [extensiones](/docs/menu/extensions.html#extensiones).
- Mantener esta documentación lo más [actualizada posible](https://gitlab.com/HomeInside/Anuket-docs).
- [Informar fallos](https://gitlab.com/HomeInside/Anuket/issues).
- Traducciones.
- [Lista de cambios](/docs/menu/changelog.html).

Para cualquier duda, comentario, sugerencia ó aporte, dirigete a la sección de [issues.](https://gitlab.com/HomeInside/Anuket/issues)
Antes de abrir un issue nuevo, revisa los ya existentes en busca de una solución (posiblemente ya planteada) para el problema que se te presenta.


## Licencia

El texto completo de la licencia puede ser encontrado en el archivo MIT-LICENSE.txt


## Contacto
[Diniremix](https://gitlab.com/diniremix)

email: *diniremix [at] gmail [dot] com*
